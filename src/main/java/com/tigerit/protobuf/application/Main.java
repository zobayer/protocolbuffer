package com.tigerit.protobuf.application;

import com.tigerit.protobuf.generated.AddressBookProtos;

import java.io.*;

/**
 * Author: zobayer
 * Date: 10/28/14.
 */
public class Main {
    private static final String FNAME = "addressbook.dat";

    public static void main(String[] args) throws IOException {
        AddressBookProtos.AddressBook.Builder addressBook = AddressBookProtos.AddressBook.newBuilder();
        BufferedReader stdin = new BufferedReader(new InputStreamReader(System.in));
        int command;
        String lineBuff;
        showMenu();
        MainLoop:
        while((lineBuff = stdin.readLine()) != null) {
            command = Integer.valueOf(lineBuff);
            switch(command) {
                case 1:
                    addressBook.addPerson(ProtoBufManager.createNewPerson(stdin, System.out));
                break;
                case 2:
                    int total = addressBook.getPersonCount();
                    System.out.println("Total " + total + " entries.");
                    for(int i = 0; i < total; i++) {
                        System.out.println("Record " + i + 1);
                        AddressBookProtos.Person person = addressBook.getPerson(i);
                        String name = person.getName();
                        String email = person.getEmail();
                        System.out.println(name + " <" + email + ">");
                        int phones = person.getPhoneCount();
                        System.out.println("Total " + phones + " phone numbers:");
                        for(int j = 0; j < phones; j++) {
                            AddressBookProtos.Person.PhoneNumber phoneNumber = person.getPhone(j);
                            System.out.println(phoneNumber);
                        }
                        System.out.println("End of record " + i + 1);
                    }
                break;
                case 3:
                    FileOutputStream output = new FileOutputStream(FNAME);
                    addressBook.build().writeTo(output);
                    output.close();
                break;
                case 4:
                    try {
                        addressBook.mergeFrom(new FileInputStream(FNAME));
                    } catch(FileNotFoundException ex) {
                        ex.printStackTrace();
                        System.out.println(">> Press 'enter' to continue...");
                    }
                break;
                case 5: break MainLoop;
                default: System.out.println(">> Unknown choice.");
            }
            showMenu();
        }
    }

    public final static void showMenu() {
        clearConsole();
        System.out.println("********************************");
        System.out.println("* 1. Add                       *");
        System.out.println("* 2. Find                      *");
        System.out.println("* 3. Save                      *");
        System.out.println("* 4. Load                      *");
        System.out.println("* 5. Exit                      *");
        System.out.println("********************************");
        System.out.print(">> Your choice: ");
    }

    public final static void clearConsole() {
        try {
            final String os = System.getProperty("os.name");
            if (os.contains("Windows")) {
                Runtime.getRuntime().exec("cls");
            } else {
                Runtime.getRuntime().exec("clear");
            }
        } catch (final Exception e) {
            e.printStackTrace();
        }
    }
}
