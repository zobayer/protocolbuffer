package com.tigerit.protobuf.application;

import com.tigerit.protobuf.generated.AddressBookProtos;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintStream;

/**
 * Author: zobayer
 * Date: 10/28/14.
 */
public class ProtoBufManager {
    public static AddressBookProtos.Person createNewPerson(BufferedReader stdin, PrintStream stdout) throws IOException {
        AddressBookProtos.Person.Builder person = AddressBookProtos.Person.newBuilder();

        stdout.print("Enter person ID: ");
        person.setId(Integer.valueOf(stdin.readLine()));

        stdout.print("Enter name: ");
        person.setName(stdin.readLine());

        stdout.print("Enter email address: ");
        person.setEmail(stdin.readLine());

        String phoneNo;
        stdout.println("Enter phone numbers. Blank line to exit:");
        while((phoneNo = stdin.readLine()) != null) {
            if(phoneNo.length() == 0) {
                break;
            }

            AddressBookProtos.Person.PhoneNumber.Builder phoneNumber = AddressBookProtos.Person.PhoneNumber.newBuilder().setNumber(phoneNo);
            person.addPhone(phoneNumber);
        }

        return person.build();
    }
}
