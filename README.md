Google Protocol Buffer
======================

## A simple example for using Google Protocol Buffer in a java application

## Installation and Compilation:
--------------------------------
1. If you haven't installed the Protocol Buffer compiler, download the package and follow the instructions in the included README file. Download location: [https://developers.google.com/protocol-buffers/docs/downloads].
2. Run ldconfig to update ld.so.cache after making sure that /usr/local/lib is listed in /etc/ld.so.conf. This prevents the use of stale ld.so.cache file.
3. Compile the *.proto files before compiling the java program.